import React, { Component } from 'react';
import axios from 'axios';
import '../styles/formula-one-api/formula-one-api.css';

// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 


class FormulaOne extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            drivers: [],
            year: [],
            driverTable: []

        }
    };

    componentDidMount() {
        const baseUrl = "https://ergast.com/api/f1/";
        const year = "2018/"
        const drivers = "drivers.json?limit=54";
        // const driverStandings = "driverStandings.json?";
        // const season = "season.json?";
        // const current = "current.json?";

        axios
        .get(
            baseUrl + year + drivers
        )
        
        .then(data => {
            this.setState({ 
                isLoaded: true,
                driverTable: data.data.MRData.DriverTable,
                drivers: data.data.MRData.DriverTable.Drivers,
                year: data.data.MRData.DriverTable.season,
            })
            /* this.state.drivers.map((driver) =>  // loop driver 
                this.setState({ 
                    driver: this.state.drivers.concat(driver),
                })
            ) */
        })
        .catch(error => console.log(error));
    }

    
    render() {

        return (
            <div className="App  formula-one-container container">
                <div className="App-header">
                    <p className="App-description">Formula One API (Ergast data)</p>
                </div>   
                <div className="driver">
                    <div className="driver-header">
                        <h2>Drivers for year: {this.state.year}</h2>
                    </div>
                    <div className="driver-wrapper" >
                        {  this.state.drivers.map((driver, index) =>
                        <div key={index} className="driver-container" >
                            <div className="driver-name">
                                <p>Name: {driver.givenName} {driver.familyName}</p>
                            </div>
                            <div className="driver-details">
                                <p>DOB: {driver.dateOfBirth}</p>
                                <p>Nationality: {driver.nationality}</p>
                                <p>Car Number: {driver.permanentNumber}</p>
                            </div>
                        </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

export default FormulaOne;