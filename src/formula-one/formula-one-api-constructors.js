import React, { Component } from 'react';
import axios from 'axios';
import '../styles/formula-one-api/formula-one-api.css';

const baseUrl = "https://ergast.com/api/f1/constructors/";
const drivers = "/drivers.json";

class FormulaOneConstructors extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            drivers: [],
            driverTable: [],
            team: "mclaren"
        }
        this.handleChangeTeam = this.handleChangeTeam.bind(this);
    };

    componentDidMount() {
        axios
            .get(
                baseUrl + this.state.team + drivers
            )
            
            .then(data => {
                this.setState({ 
                    isLoaded: true,
                    driverTable: data.data.MRData.DriverTable,
                    drivers: data.data.MRData.DriverTable.Drivers,
                })
            })
            .catch(error => console.log(error));
    }

    handleChangeTeam(event) {
        event.preventDefault();
        this.setState({
            team: event.target.value,
        });
    }

    handleSubmit(event) {  
        event.preventDefault(); 
        this.setState({
            isLoaded: false,
        });
        
        axios
            .get(
                baseUrl + this.state.team + drivers
            )
            .then(data => {
                this.setState({
                    isLoaded: true,
                    driver: data.data.MRData.StatusTable.driverId,
                    driverDetails: data.data.MRData.StatusTable.Status,
                })
            })
            .catch(error => console.log(error))
    }
    
    render() {
        console.log(this.state)
        return (
            <div className="App formula-one-constructors-container container">
                <div className="App-header">
                    <p className="App-description">Formula One API (Ergast data)</p>
                    <p>F1 constructors and drivers</p>
                </div>   
                <form className="form" onSubmit={this.handleSubmit}> 
                    <label className="input-container">
                        <p>Reference Team: </p>
                        <input type="text" value={this.state.team} onChange={this.handleChangeTeam} className="input-box"/>
                    </label>
                    <input type="submit" value="Submit"  />
                </form> 
                <div className="driver">
                    <div className="driver-header">
                        <h2>Drivers for {this.state.team}</h2>
                    </div>
                    <div className="driver-wrapper" >
                        { this.state.drivers.map((driver, index) =>
                        <div key={index} className="driver-container" >
                            <div className="driver-name">
                                <p>Name: {driver.givenName} {driver.familyName}</p>
                            </div>
                            <div className="driver-details">
                                <p>DOB: {driver.dateOfBirth}</p>
                                <p>Nationality: {driver.nationality}</p>
                                <p>Car Number: {driver.permanentNumber}</p>
                            </div>
                        </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

export default FormulaOneConstructors;