import React, { Component } from 'react';
import axios from 'axios';
import '../styles/formula-one-api/formula-one-api.css';

class FormulaOne extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            drivers: [],
            year: "",
            driverTable: [],
            circuits:[],
            circuitsTable: [],
            constructors:[],
            constructorsTable: [],
            results: [],
            resultsTable: []
        }
    };

    componentDidMount() {
        const baseUrl = "https://ergast.com/api/f1/";
        const year = "2018"
        const drivers = "/drivers.json?limit=54";
        const circuits = "/circuits.json?limit=54";
        const constructors = "/constructors.json?limit=54";
        const results = "/results.json?limit=500";        

        axios
            .get(
                baseUrl + year + drivers
            )
            
            .then(data => {
                this.setState({ 
                    isLoaded: true,
                    driverTable: data.data.MRData.DriverTable,
                    drivers: data.data.MRData.DriverTable.Drivers,
                    year: data.data.MRData.DriverTable.season,
                })
            })
            .catch(error => console.log(error));

        axios
            .get(
                baseUrl + year + circuits
            )
            
            .then(data => {
                this.setState({ 
                    isLoaded: true,
                    circuits: data.data.MRData.CircuitTable.Circuits,
                    circuitsTable: data.data.MRData.CircuitTable,
                })
            })
            .catch(error => console.log(error));


        axios
            .get(  
                baseUrl + year + constructors
            )
            .then((data) => {
                    this.setState({
                        constructors: data.data.MRData.ConstructorTable.Constructors,
                        constructorsTable: data.data.MRData.ConstructorTable,
                    });
                }
            )
            
            .catch(error => console.log(error));

        axios
            .get(
                baseUrl + year + results
            )
            .then(
                (data) => {
                    this.setState({
                        results: data.data.MRData.RaceTable.Races,
                        resultsTable: data.data.MRData.RaceTable,
                        
                    });
                }
            )
            .catch(error => console.log(error));

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({year: event.target.value}); // build search year
    }

    handleSubmit(event) {  // submit search year for data
        event.preventDefault(); 

        axios
            .get("https://ergast.com/api/f1/" + this.state.year + "/drivers.json?limit=54")
            .then(
                (data) => {
                    this.setState({
                        drivers: data.data.MRData.DriverTable.Drivers,
                        driverTable: data.data.MRData.DriverTable,
                    });
                }
            )
            .catch(error => console.log(error))
        axios
            .get("https://ergast.com/api/f1/" + this.state.year + "/circuits.json?limit=54")
            .then(
                (data) => {
                    this.setState({
                        circuits: data.data.MRData.CircuitTable.Circuits,
                        circuitsTable: data.data.MRData.CircuitTable,
                    });
                }
            )
            .catch(error => console.log(error))
        axios
            .get("https://ergast.com/api/f1/" + this.state.year + "/constructors.json?limit=54")
            .then(
                (data) => {
                    this.setState({
                        constructors: data.data.MRData.ConstructorTable.Constructors,
                        constructorsTable: data.data.MRData.ConstructorTable,
                    });
                }
            )
            .catch(error => console.log(error))
        axios
            .get("https://ergast.com/api/f1/" + this.state.year + "/results.json?limit=500")
            .then(
                (data) => {
                    this.setState({
                        results: data.data.MRData.RaceTable.Races,
                        resultsTable: data.data.MRData.RaceTable,
                    });
                }
            )
            .catch(error => console.log(error))
    }
    
    
    
    renderPosition(raceResult) {
        let trophySVG = 
            <g>
                <path d="M144.9,200c-29.3,0-58.7,0-88,0c0-3.2,0-6.4,0-10c3.6,0,6.8,0,10.4,0c0-7.7,0.1-14.9-0.1-22.2c-0.1-2.8,0.9-4.5,2.9-6.4
                    c12.8-12.2,21.3-26,16.5-44.9c-2.8-0.2-5.4-0.3-8.1-0.5c0-4.1,0-7.8,0-11.6c1.4-0.2,2.7-0.4,4.3-0.6c-2.7-2.6-5.4-4.6-7.3-7.1
                    c-2.5-3.2-5.6-4.3-9.4-4.9C52.1,89.2,40.2,82.7,31,71.8C21.7,60.9,17.1,48,16.1,34c-0.7-10.9,0.1-22,0.2-33.8
                    c16.4,4,31.4,7.7,46.2,11.3c0.2,2.5,0.4,4.2,0.6,6c24.8,0,49.3,0,73.9,0c0.2-2.3,0.3-4.3,0.5-6.4c0.6-0.2,1-0.4,1.4-0.5
                    C153,7.1,167.2,3.5,181.3,0c0.6,0,1.2,0,1.8,0c0,13,1.3,26.1-0.2,38.9c-3.4,28.6-23.6,48.4-51.9,52.7c-1,0.1-2.2,0.2-2.8,0.8
                    c-3.7,3.7-7.2,7.5-10.9,11.5c1.6,0.1,2.9,0.2,4.2,0.3c0,4,0,7.9,0,11.9c-2.5,0.2-4.6,0.3-6.5,0.5c-0.7,3.8-2.1,7.3-1.7,10.5
                    c1.4,13.8,7.7,25.3,18.1,34.5c2.1,1.9,2.9,3.6,2.9,6.4c-0.2,7.3-0.1,14.7-0.1,22.4c4,0,7.3,0,10.8,0
                    C144.9,193.8,144.9,196.9,144.9,200z M27.9,15.2c-3.3,33.1,11.5,58.1,34,62.7c-3.3-13.7-6.7-27.8-10-41.8c-3,0.7-5.2-1.5-8.1-9.1
                    c2.4-1.7,4.7-3.4,7.9-5.7C43.1,19,35.7,17.1,27.9,15.2z M172,14.5c-7.8,2-15.3,3.9-24,6.1c3.5,2.4,6,4.1,8.6,5.9
                    c-2.1,3.4-4,6.5-5.9,9.6c-1.2-0.4-2.3-0.7-2.6-0.8c-3.4,14.3-6.7,28.4-9.9,42.2C160.8,72.7,175.8,45.6,172,14.5z"/>
            </g>;


        if(raceResult.position === '1') {
            return (
                <div className="driver-number-wrapper">
                    <svg version="1.1" id="gold-trophy" className="trophy" x="0px" y="0px" viewBox="0 0 200 200">
                      {trophySVG}
                    </svg>
                    <div className="driver-number">
                        <p> {raceResult.position}</p>
                        <p className="driver-number-shadow">{raceResult.position}</p>
                    </div>
                </div>
            );
        } else if (raceResult.position === '2') {
            return (
                <div className="driver-number-wrapper">
                  <svg version="1.1" id="silver-trophy" className="trophy" x="0px" y="0px" viewBox="0 0 200 200">
                    {trophySVG}
                    </svg>
                    <div className="driver-number">
                        <p> {raceResult.position}</p>
                        <p className="driver-number-shadow">{raceResult.position}</p>
                    </div>
                </div>
            );
        } else if (raceResult.position === '3') {
            return (
                <div className="driver-number-wrapper">
                  <svg version="1.1" id="bronze-trophy" className="trophy" x="0px" y="0px" viewBox="0 0 200 200">
                    {trophySVG}
                    </svg>
                    <div className="driver-number">
                        <p>{raceResult.position}</p>
                        <p className="driver-number-shadow">{raceResult.position}</p>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="driver-number-wrapper">
                    <div className="driver-number">
                        <p>{raceResult.position}</p>
                        <p className="driver-number-shadow">{raceResult.position}</p>
                    </div>
                </div>
            );
        }
    }

    renderFinished(raceResult) {
        if(raceResult.status === 'Finished') {
            return (
                <div className="driver-status-container">
                    <div className="driver-status"><p><b>Finished Race:</b></p></div>
                    <div className="driver-status-number result-number">
                        <p>{raceResult.status}</p>
                    </div>
                </div>
           );
        } else if(raceResult.status[0] === '+') {
            return (
                <div className="driver-status-container">
                    <div className="driver-status"><p><b>Laps behind:</b></p></div>
                    <div className="driver-status-number result-number">
                        <p>{raceResult.status}</p>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="driver-status-container">
                    <div className="driver-status"><p><b>Retired due to:</b></p> </div>
                    <div className="driver-status-number result-number">
                        <p>{raceResult.status}</p>
                    </div>
                </div>
            );
        }
    }
    

    render() {
        console.log(this.state);

        let numberplateSVG =
            <svg version="1.1" id="numberPlate" x="0px" y="0px" viewBox="0 0 200 200"  >
            <path className="st1" d="M179.7,191.7H20.3c-6.6,0-12-5.4-12-12V20.3c0-6.6,5.4-12,12-12h159.5c6.6,0,12,5.4,12,12v159.5 C191.7,186.3,186.3,191.7,179.7,191.7z"/>
        </svg>;

        return (
            <div className="App formula-one-container">
                <div className="App-header">
                    <p className="App-description">Formula One API (Ergast data)</p>
                </div>
                <form className="form" onSubmit={this.handleSubmit}> 
                    <label className="input-container">
                        <p>Reference By Year: </p>
                        <input type="text" value={this.state.year} onChange={this.handleChange} className="input-box"/>
                    </label>
                    <input type="submit" value="Submit" />
                </form> 
                <div className="driver">
                    <div className="driver-header">
                        <h2>Drivers for year: {this.state.year}</h2>
                    </div>
                    <div className="driver-wrapper" >
                        {  this.state.drivers.map((driver, index) =>
                        <div key={index} className="driver-container" >
                            <div className="driver-name">
                                <p><b>Name:</b> {driver.givenName} {driver.familyName}</p>
                            </div>
                            <div className="driver-details">
                                <p><b>DOB:</b> {driver.dateOfBirth}</p>
                                <p><b>Nationality:</b> {driver.nationality}</p>
                                <p><b>Permanent Number (if after 2014):</b> </p>
                                <div className="driver-number">
                                    <p>{driver.permanentNumber}</p>
                                    <p className="driver-number-shadow">{driver.permanentNumber}</p>
                                   {numberplateSVG}
                                </div>
                            </div>
                        </div>
                        )}
                    </div>
                </div>
                <div className="circuit">
                    <div className="circuit-header">
                        <h2>Circuits for year: {this.state.year}</h2>
                    </div>
                    <div className="circuit-wrapper" >
                        <div className="circuit-grid" >
                            {  this.state.circuits.map((circuit, index) =>
                            <div key={index} className="circuit-container" >
                                <div className="circuit-name">
                                    <p><b>Circuit Name:</b> {circuit.circuitName} </p>
                                </div>
                                <div className="circuit-details">
                                    <a href={circuit.url} className="circuit-link">
                                        <b>Wiki about</b> {circuit.circuitName}
                                    </a>
                                </div>
                                <div className="circuit-map">
                                </div>
                            </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="constructor">
                    <div className="constructor-header">
                        <h2>Constructors for year: {this.state.year}</h2>
                    </div>
                    <div className="constructor-wrapper" >
                        <div className="constructor-grid" >
                            {  this.state.constructors.map((constructor, index) =>
                            <div key={index} className="constructor-container" >
                                <div className="constructor-name">
                                    <p><b>Team Name:</b> {constructor.name} </p>
                                </div>
                                <div className="constructor-details">
                                    <a href={constructor.url} className="circuit-link">
                                        <p><b>Wiki about</b> {constructor.name}</p>
                                    </a>
                                    <p><b>Nationality:</b> {constructor.nationality} </p>
                                </div>
                            </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="races">
                    <div className="races-header">
                        <h2>Races and results for: {this.state.year}</h2>
                    </div>
                    <div className="races-wrapper" >
                        <div className="races-grid" >
                            {  this.state.results.map((race, index) =>
                            <div key={index} className="races-container" >
                                <div className="races-header">
                                    <div className="races-name">
                                        <p><b>Round:</b> <span>{race.round}</span> </p>
                                        <p><b>Round Name:</b> <span>{race.raceName}</span> </p>
                                    </div>
                                    <div className="races-details">
                                        <a href={race.url} className="races-link">
                                            <b>Wiki about</b> {race.raceName}
                                        </a>
                                    </div>
                                </div>
                                <div className="race-results-container">
                                    { race.Results.map((raceResult, index) =>
                                        <div key={index}  className="race-results">

                                            {this.renderPosition(raceResult)}
                                          
                                          
                                            <div className="driver-details">
                                            <p><b>Constructor:</b> {raceResult.Constructor.name}</p>
                                            <p><b>Name:</b> {raceResult.Driver.givenName} {raceResult.Driver.familyName}</p>
                                                <div className="driver-number">
                                                    <p>{raceResult.number}</p>
                                                    <p className="driver-number-shadow">{raceResult.number}</p>
                                          
                                                </div>
                                            </div>
                                            <div className="driver-grid-container">
                                                <div className="driver-grid"><p><b>Grid Position:</b></p></div>
                                                <div className="driver-grid-number result-number"> 
                                                    <p>{raceResult.grid}</p>
                                                </div>
                                            </div>
                                            <div className="driver-points-container">
                                                <div className="driver-points"><p><b>Points:</b></p></div>
                                                <div className="driver-points-number result-number">
                                                    <p>{raceResult.points}</p>
                                                </div>
                                            </div>
                                            <div className="driver-laps-container">
                                                <div className="driver-laps"><p><b>Laps:</b></p></div>
                                                <div className="driver-laps-number result-number">
                                                    <p>{raceResult.laps}</p>
                                                </div>
                                            </div>
                                            {this.renderFinished(raceResult)}
                                        </div>
                                    )}
                                </div>
                            </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormulaOne;