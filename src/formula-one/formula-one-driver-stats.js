import React, { Component } from 'react';
import axios from 'axios';
import CirclesChart from './charts/circles.js';
import BarsChart from './charts/bars.js';
import '../styles/formula-one-api/formula-one-driver-stats.css';

class FormulaOneDriverStats extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            driver: "hamilton",
            year: "2018",
            driverDetails: [], 
        }
    };

    componentDidMount() {
        const baseUrl = "https://ergast.com/api/f1/";
        const year = this.state.year;
        const drivers = "/drivers/";
        const driver = this.state.driver;
        const status = "/status.json";
       
        axios
            .get(
                baseUrl + year + drivers + driver + status
            )
            
            .then(data => {
                this.setState({ 
                    isLoaded: true,
                    year: data.data.MRData.StatusTable.season,
                    driver: data.data.MRData.StatusTable.driverId, 
                    driverDetails: data.data.MRData.StatusTable.Status, 
                })
            })
            .catch(error => console.log(error));

        this.handleChangeYear = this.handleChangeYear.bind(this);
        this.handleChangeDriver = this.handleChangeDriver.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeYear(event) {
        event.preventDefault();
        this.setState({
            year: event.target.value,
        });
    }
    handleChangeDriver(event) {
        event.preventDefault();
        this.setState({
            driver: event.target.value
        });
    }

    handleSubmit(event) {  // submit search year for data
        event.preventDefault(); 
        this.setState({
            isLoaded: false,
            driverDetails: null, 
        });
        
        axios
            .get("https://ergast.com/api/f1/" + this.state.year + "/drivers/" + this.state.driver + "/status.json")
            .then(
                (data) => {
                    this.setState({
                        isLoaded: true,
                        driver: data.data.MRData.StatusTable.driverId, 
                        year: data.data.MRData.StatusTable.season,
                        driverDetails: data.data.MRData.StatusTable.Status, 
                    });
                }
            )
            .catch(error => console.log(error))
    }

    render() {       
        let data;
        if (this.state.isLoaded) {  // check if data has loaded

            if ( this.state.driverDetails && this.state.driverDetails.length) {  // check if not empty
                data = 
                    <div className="charts">
                        <CirclesChart data={this.state.driverDetails} />
                        <BarsChart data={this.state.driverDetails} />
                    </div>
            } else {
                data =  
                    <div>
                        <div className="loader-container-static"></div>  
                        <div><p>No data available</p></div>    
                    </div>   
            }
        } else {
            data =  
                <div>
                    <div className="loader-container"></div>  
                    <div><p>Loading Data</p></div>    
                </div>      
        }
        
        return (
            <div className="App formula-one-driver-stats container">
                <div className="App-header">
                    <p className="App-description">Formula One API Driver Finishes by season (Ergast data)</p>
                </div>
                <form className="form" onSubmit={this.handleSubmit}> 
                    <label className="input-container">
                        <p>Reference By Year: </p>
                        <input type="text" value={this.state.year} onChange={this.handleChangeYear} className="input-box"/>
                        <p>Reference By Driver Surname: </p>
                        <input type="text" value={this.state.driver} onChange={this.handleChangeDriver} className="input-box"/>
                    </label>
                    <input type="submit" value="Submit"  />
                </form> 
                <div className="driver-stats">
                    {data}
                </div>
            </div>
        )
    }
}

export default FormulaOneDriverStats;