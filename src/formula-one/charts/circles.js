import React, { Component } from 'react';
import '../../styles/formula-one-api/formula-one-driver-stats.css';

class CirclesChart extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: this.props.data,
            total: [],
            integer: []
        }
    }
    
    componentDidMount() {
        this.state.data.map((driverStats) =>
            this.state.total.push(driverStats.count),
        );

        this.state.total.map((driverTotal) =>
            this.state.integer.push(driverTotal),
        );

        console.log(this.state)
    }

    render() {     
        return (
            <div className="graph">
                <div className="title"><p>Driver Finishes</p></div>
                <div className="graph-container">
                    {this.state.data.map((driverStats, index) =>
                    <div key={index} className="graph-stats" >
                        <div className="count">{driverStats.count}</div>
                        <svg x="0" y="0" viewBox="0 0 100 100"  width="100%" aria-labelledby="title desc" role="img">
                            <g className="graph-dot">
                                <circle cx="50" cy="50" r="45"></circle>
                                <circle cx="50" cy="50" r={100 / 100 * driverStats.count * 2 }></circle>
                                <circle cx="50" cy="50" r="0.5"></circle>
                            </g>
                        </svg>
                        {driverStats.status}
                    </div>
                    )}
                </div>
            </div>
        )
    }
}

export default CirclesChart;