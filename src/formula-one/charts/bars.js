import React, { Component } from 'react';
import '../../styles/formula-one-api/formula-one-driver-stats.css';

class BarsChart extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            data: this.props.data
        }
    }

    render() {     
        return (
            <div className="chart">
                <div className="title"><p>Results in svg bars</p></div>
                {this.state.data.map((driverStats, index) =>
                    <svg key={index} x="0" y="0" className="chart-stats" viewBox="0 0 400 38"  width="100%" aria-labelledby="title desc" role="img">
                        <g className={"bar bar" + index}>
                            <text className="count" x={(160 + driverStats.count * 3 / 1 + 1 + driverStats.count * 3 / 1.5 + 1 + driverStats.count * 3 / 2 + 1 + driverStats.count * 3 / 4 + 7)}  y={20} dy=".35em">{driverStats.count}</text>
                            <rect className="finishedFlag" x="160" y={10} width={driverStats.count * 3 / 1} height="18"></rect>
                            <rect x={(160 + driverStats.count * 3 / 1 + 1)} y={10} width={driverStats.count * 3 / 1.5} height="18"></rect>
                            <rect x={(160 + driverStats.count * 3 / 1 + 1 + driverStats.count * 3 / 1.5 + 1)} y={10} width={driverStats.count * 3 / 2} height="18"></rect>
                            <rect x={(160 + driverStats.count * 3 / 1 + 1 + driverStats.count * 3 / 1.5 + 1 + driverStats.count * 3 / 2 + 1)} y={10} width={driverStats.count * 3 / 4 } height="18"></rect>
                            <rect x={(160 + driverStats.count * 3 / 1 + 1 + driverStats.count * 3 / 1.5 + 1 + driverStats.count * 3 / 2 + 1 + driverStats.count * 3 / 4 + 1)} y={10} width="1" height="18"></rect>
                            <text  x="7"  y={20} dy=".35em">{driverStats.status}</text>
                        </g>
                    </svg>
                )}
            </div>
        )
    }
}

export default BarsChart;