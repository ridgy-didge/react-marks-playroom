import React, { Component } from 'react';

class AnimateMe extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animate: false,
    };
  }

  componentDidMount() {
    this.setState({ animate: true });
  }

  render() {
    return (
        <div
            style={ {
              background: '#eee',
              border: '1px solid black',
              height: this.state.animate ? 250 : 50,
              margin: 20,
              padding: 20,
              transition: 'all 2s',
            } }
        >
        <p>hello</p>
        </div>
    );
  }
}
export default AnimateMe;