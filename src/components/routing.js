import React, { Component } from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom'
import '../styles/base.css';

import Home from './home';
import MainNav from './main-nav';
import AllProductDetails from '../wp-components/all-products-details';
import AllCategoriesDetails from '../wp-categories/all-categories';
import StarWars from '../star-wars/Starwars';
import FormulaOne from '../formula-one/formula-one-api';
import SingleProductDetails from '../wp-products/wp-product';
import WPProducts from '../wp-products/wp-products';
import F1DriverStats from '../formula-one/formula-one-driver-stats';
import FormulaOneConstructors from '../formula-one/formula-one-api-constructors';
import Blender from '../blender/Blender';
import spaceship2019 from '../blender/spaceship2019';

// sub nav for wp-components
// import WPRouting from './wp-components/routing/wp-routing'

// import 'bootstrap';    // import all bootstrap js files
// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 


class Routing extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {

    return (
      <Router>
        <div>
          <MainNav/>
          {/* Route components are rendered if the path prop matches the current URL */}
          <Route exact path="/" component={Home} />
          <Route exact path="/starwars-api" render={() => <StarWars/>} />
          <Route exact path="/react-with-wp-api/phone-acf-details" render={() => <AllProductDetails/>} />
          <Route exact path="/wp-categories/all-categories" render={() => <AllCategoriesDetails/>} />
          <Route exact path="/formula-one-api" render={() => <FormulaOne/>} />
          {/* <Route path="/wp-content/product/:slug" render={() => <ProductDetails state={this.state.productSlug}/>} /> */}
          <Route path="/wp-products/phones/:slug" component={SingleProductDetails}/>
          {/* <Route exactpath="/wp-products" render={() => <WPProducts/>}  />  */} 
          <Route exact path="/wp-products/phones" component={WPProducts} />    
          <Route exact path="/formula-one-api/f1-driver-stats" component={F1DriverStats} />    
          <Route exact path="/formula-one-api/f1-constructors" component={FormulaOneConstructors} />    
          <Route exact path="/blender" component={Blender} />    
          <Route exact path="/blender/spaceship2019" component={spaceship2019} />    
        </div>
      </Router>
    )
  }
}

export default Routing;
