import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/base.css';

const Nav = () => (
    <div className="navbarNav" id="">
        <ul className="navbar-nav">
            {/* Link components are used for linking to other views */}
            <li><Link className="nav-link active"  to="/">Home</Link></li>
            <li><Link className="nav-link"  to="/starwars-api">Star Wars API</Link></li>
            <li><Link className="nav-link dropdown-toggle" data-toggle="dropdown" to="/formula-one-api" role="button" aria-haspopup="true" aria-expanded="false">Formula One API</Link>
            <ul className="dropdown-menu">
                <li><Link className="dropdown-item"  to="/formula-one-api">Seasons</Link></li>
                <li className="divider" >|</li>
                <li><Link className="dropdown-item"  to="/formula-one-api/f1-driver-stats">Driver Finishes Stats</Link></li>
            </ul>
            </li>

        <li><Link className="nav-link dropdown-toggle" data-toggle="dropdown" to="/react-with-WP-API" role="button" aria-haspopup="true" aria-expanded="false">Wordpress API</Link>
            <ul className="dropdown-menu">
            <li><Link  className="dropdown-item" to="/react-with-wp-api/phone-acf-details">Wordpress API ACF</Link></li>
            <li className="divider" >|</li>
            <li><Link  className="dropdown-item" to="/wp-categories/all-categories">Wordpress Categories</Link></li>
            <li className="divider" >|</li>
            <li><Link  className="dropdown-item" to="/wp-products/phones">Wordpress product list and links</Link></li>
            </ul>
        </li>
        <li><Link className="nav-link dropdown-toggle" data-toggle="dropdown" to="/blender" role="button" aria-haspopup="true" aria-expanded="false">Blender</Link>
            <ul className="dropdown-menu">
                <li><Link className="dropdown-item"  to="/blender/spaceship2019">Spaceship 2019</Link></li>
            </ul>
        </li>
        </ul>
    </div>
)

class MainNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isHidden: true,
            width: window.innerWidth
        }
    
        // This binding is necessary to make `this` work in the callback
        this.toggleHidden = this.toggleHidden.bind(this);
    }

    // check width
    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }
    
    // make sure to remove the listener
    // when the component is not mounted anymore
    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    } 
    
    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    
    toggleHidden () {
        this.setState({
          isHidden: !this.state.isHidden
        })
    }

    render() {
        const { width } = this.state;
        const isMobile = width <= 768;  // 768 matches bootstrap media query size
      
        if (isMobile) {
            return (
                <div className="nav-container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <button onClick={this.toggleHidden} className="navbar-toggler" type="button">
                            <span className="navbar-toggler-icon">
                            </span>
                        </button>
                        {!this.state.isHidden && <Nav/>}
                    </nav>
                </div>   
            )
        } else {
            return (
                <div className="nav-container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <Nav/>
                    </nav>
                </div>
            );
        }
    }
}

export default MainNav;
