import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import BtnCirc from '../components/svgs/btn-circ';

class HomeNav extends Component {

    render() {
        const data = [
            { key: 1, slug: 'wordpress-api', title: 'Wordpress API', img_url:  require('../images/logos/wordpress.svg'), url: '/react-with-WP-API', desc: 'React with Wordpress API'},
            { key: 2, slug: 'starwars-api', title: 'Star Wars API', img_url: require('../images/starwars/logo.svg'), url: '/starwars-api', desc: 'React with Star Wars API (SWAPI)'},
            { key: 3, slug: 'formulaone-api', title: 'Formula One API', img_url: require('../images/logos/f1.svg'), url: '/formula-one-api',desc: 'React with Formula One API (Ergast)'},
            { key: 4, slug: 'blender-images', title: 'Blender 3D Images', img_url: require('../images/blender/spaceship-2019/SPACESHIP2019-3-smoke@0,5x.png'), url: '/blender',desc: 'Blender images i have created'},
        ];
        const listItems = data.map((item) =>
            <div key={item.key} className={'home-nav-item ' + item.slug}>
                <div className="home-nav-item-img">
                    <img alt={item.title} src={item.img_url} />
                </div>
                <div className="home-nav-item-slide">
                    <div className="home-nav-item-desc">
                        <p>{item.desc}</p>
                    </div>
                    <div className="home-nav-item-btn">
                        <Link to={item.url} ><BtnCirc></BtnCirc></Link>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="home-nav container">
                {listItems}          
            </div>
        )
    }
}

export default HomeNav;