import React, { Component } from 'react';
import '../../styles/svgs/btn-circ.css';

class BtnCirc extends Component {
  constructor() {
    super();
    this.state = { };
  }

  render()  {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500">
        <title>btn-circles</title>
        <g id="circles">
          <circle class="btn-circ-1" cx="250" cy="250" r="240"/>
          <circle class="btn-circ-2" cx="250" cy="250" r="240"/>
          <circle class="btn-circ-1" cx="250" cy="250" r="212"/>
          <circle class="btn-circ-3" cx="250" cy="250" r="212"/>
          <circle class="btn-circ-1" cx="250" cy="250" r="170"/>
          <circle class="btn-circ-4" cx="250" cy="250" r="170"/>
          <circle class="btn-circ-1" cx="250" cy="250" r="121"/>
          <circle class="btn-circ-5" cx="250" cy="250" r="121"/>
          <g id="enter">
            <path d="M184.16,236.26H207v7h-14.8v3.43H207v5.57h-14.8v3.43H207v7H184.16Z"/>
            <path d="M209.41,236.26h10.17l8.51,13.12V236.26h7.63v26.39h-7.63l-10.69-17v17h-8Z"/>
            <path d="M238.26,236.26h24.61v6.54h-8.11v19.85h-8.39V242.8h-8.11Z"/>
            <path d="M265.13,236.26h22.79v7h-14.8v3.43h14.8v5.57h-14.8v3.43h14.8v7H265.13Z"/>
            <path d="M316,262.65h-8V252.24h-9.64v10.41h-8V236.26h20.41a6.05,6.05,0,0,1,1.65.2,5.38,5.38,0,0,1,1.46.73,4.7,4.7,0,0,1,2.09,4.08v4.11a5.29,5.29,0,0,1-.92,2.3,5.72,5.72,0,0,1-1.5,1.29c-.21.14-.55.34-1,.61a8.46,8.46,0,0,1,.76.44,7.39,7.39,0,0,1,1.21,1,5.1,5.1,0,0,1,1.45,3.39Zm-17.63-15.9h9.64v-3.51h-9.64Z"/>
          </g>
        </g>
      </svg>
    );
  }
}

export default BtnCirc;

