import React, { Component } from 'react';
import '../styles/home/home-nav.css';
import HomeNav from './home-nav';

class Home extends Component {

    render() {

        return (
            <div className="home home-container">
                <div className="home-welcome">
                    <p>I am Mark Almond a Frontend developer based in Southampton.</p>
                    <p>This is my site for testing codes and apis.</p>
                    <p>Currently working with Wordpress, React and Magento 2.</p>
                </div>
                <HomeNav/>
            </div>
        )
    }
}

export default Home;