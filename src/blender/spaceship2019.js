import React, { Component } from 'react';
import '../styles/blender-app/Blender.css';

// images
import spaceship20193Smoke from '../images/blender/spaceship-2019/SPACESHIP2019-3-smoke.png';

import spaceship20193Small from '../images/blender/spaceship-2019/SPACESHIP2019-3@0,5x.jpg';
import spaceship20193WireSmall from '../images/blender/spaceship-2019/spaceship-2019-wire@0,5x.png';
import spaceship20193SmokeSmall from '../images/blender/spaceship-2019/SPACESHIP2019-3-smoke@0,5x.png';
import spaceship20193TextureSmall from '../images/blender/spaceship-2019/BODY-front@0,5x.png';


class Spaceship2019 extends Component {
  constructor() {
    super();
    this.state = {
      show: "modal hide"
    };
  }

  showModal = () => {
    this.setState({ show: "modal show" });
  };

  hideModal = () => {
    this.setState({ show: "modal hide" });
  };

  render()  {
    return (
      <div className="blender container-fluid">
        <div className="title-container">
          <p>Spaceship Racer: My space racer build from military/industrial parts.</p>
        </div>
        <div className="blender container">
          <div className="area-container">
            <div className="img-container">
              <img alt="blender spaceship 3 2019" src={spaceship20193Small}></img>
            </div>
            <div className="desc-container">
              Spaceship 3 rendered with Cycles render.
            </div>
          </div>
          <div className="area-container">
            <div className="img-container">
              <img alt="blender spaceship 3 2019 Texture" src={spaceship20193TextureSmall}></img>
            </div>
            <div className="desc-container">
              Spaceship 3 Texture created in Photoshop CC 2019.
            </div>
          </div>
          <div className="area-container">
            <div className="img-container">
              <img alt="blender spaceship 3 2019 Wireframe" src={spaceship20193WireSmall}></img>
            </div>
            <div className="desc-container">
              Spaceship 3 wireframe inside Blender.
            </div>
          </div>
          <div className="main-container">
            <div className={"modal " + this.state.show} >
              <div className="modal-main">
                <img alt="blender spaceship 3 2019 Smoke" src={spaceship20193Smoke}></img>
                <div className="close-holder">
                  <svg onClick={this.hideModal} viewBox="0 0 500 500">
                    <line x1="50" y1="50" x2="450" y2="450" />
                    <line x1="50" y1="450" x2="450" y2="50" />
                  </svg> 
                </div>
              </div>
            </div>
            <div className="img-container">
              <img onClick={this.showModal} alt="blender spaceship 3 2019 Smoke" src={spaceship20193SmokeSmall}></img>
            </div>
            <div className="desc-container">
              Spaceship 3 with Blender Smoke Particles.
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Spaceship2019;