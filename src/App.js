import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './styles/base.css';
import Routing from './components/routing';

import "../node_modules/jquery/dist/jquery.min.js";
// import 'popper.js/dist/popper.min';
// import "../node_modules/bootstrap/dist/js/bootstrap.min.js";

// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 

class App extends Component {


  componentDidMount() {
  }

  render() {
    return (
      <div className="App">
       <header className="App-header">
          <h1 className="App-title">React</h1>
          <p className="App-description">React Tests with wordpress api and others</p>
        </header>        
        <BrowserRouter>
          <Routing />
        </BrowserRouter>
      </div>
    )
  }
}

export default App;
