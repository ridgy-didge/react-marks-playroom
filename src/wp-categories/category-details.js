import React, { Component } from 'react';
import './styles/App.css';
import axios from 'axios';

// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 



class SingleProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }


  componentDidMount() {
    const accessKey = "?access_token=t4iea9tUM3QBTQcUGYojRM44";
    const category = "category/";
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
      .get(
        baseUrl + category + accessKey
      )
      .then(res => {
        const products = res.data;
        this.setState(
          {products}
        );

        console.log(this.state.products);
      })
      .catch(error => console.log(error));
  }

  render() {
    console.log(this.state.products)
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">React: Wordpress API CMS, Bootstrap 4 scss</h1>
          <p className="App-description">React with Bootstrap 4 scss added, Data from my hosted Wordpress API setup for testing. OAuth for verification, ACF data included.</p>
        </header>
          {
          this.state.products.map(product =>
            product.parent === 75 && // 75 is the product parent page

          <div key={ product.id }  className="body-container container">
           
            <div key={ product.title.rendered } className="product-container container">
              { product.acf.product_details.map((productDetails, index) =>
              <div key={ product.title.rendered } className="title-container container">
                <div className="image-container">
                  <div key={ index } className="image-holder">
                    { productDetails.product_images.map((productImages, index) =>
                      <img key={ index } src={productImages.image.url} alt={product.title.rendered} />
                    )}
                  </div>
                </div>
                <div className="product-description-container">
                  <div className="title-holder">
                    <p>{ product.title.rendered }</p>
                  </div>
                  <div className="description-holder">
                    <p>{ product.content.rendered }</p>
                  </div>
                  <div className="product-options-holder">              
                    <div className="product-colours-holder">
                      <div className="product-options-title-holder">
                        <div  className="product-title colour-title">
                          <p>Colour Options</p>
                        </div>
                      </div>
                      <div className="colour-options-holder">
                        <div className="colour-option">
                          { productDetails.product_colours.map((productColour, index) =>
                          <p key={productColour} >{productColour.colour }</p>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="product-memory-holder">
                      <div className="product-options-title-holder">
                        <div className="product-title memory-title">
                          <p>Memory Options</p>
                        </div>
                      </div>
                      <div className="memory-options-holder">
                        <div className="memory-option">
                        { productDetails.product_memory.map((productMemory, index) =>
                          <p>{ productMemory.memory_options }GB</p>
                        )}
                        </div>
                      </div>
                    </div>
                    
                    <div className="price-holder">
                      <p>£{ productDetails.price } </p>
                    </div>
                  </div>
                </div>
              </div>
            )}
            </div>
          </div>
        )}
      }
      </div>
    )
  }
}

export default SingleProductDetails;
