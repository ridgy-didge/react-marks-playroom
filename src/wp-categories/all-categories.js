import React, { Component } from 'react';
import axios from 'axios';
import '../styles/all-products-details/all-products-details.css';

// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 

class AllCategoriesDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: []
    };
  }
  componentDidMount() {
    const accessKey = "?access_token=t4iea9tUM3QBTQcUGYojRM44";
    const categories = "categories/";
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
      .get(
        baseUrl + categories + accessKey
      )
      .then(res => {
        this.setState({ 
          categories: res.data
        });
      })
      .catch(error => console.log(error));
  }
  render() {
    return (
      <div className="all-categories-container container">
        <div>
          <h2 className="title">Wordpress Categories</h2>
        </div>
        { this.state.categories.map((category, index) =>
          // category.parent === 75 && // 75 is the product parent page

          <div key={index }  className="body-container ">
            <div className="title-holder">
              <p>Category: { category.name }</p>
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default AllCategoriesDetails;