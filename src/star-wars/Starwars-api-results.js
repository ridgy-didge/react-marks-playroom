import React, { Component } from 'react';
import '../styles/starwars-app/Starwars.css';

class StarwarsApiResults extends Component {
  constructor() {
    super();
    this.state = { 
    };
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className="results-container d-flex justify-content-center" ref={this.starwarsfont}>
        {this.props.people.slice(0, 1).map( (person, index) => (
          <div key={index} className="d-flex flex-column col justify-content-between">
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p>name:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p>{person.name}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p>height:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p>{person.height}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p >mass:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.mass}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p>skin colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.skin_color}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p >eye colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.eye_color}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p >hair colour:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.hair_color}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p >birth year:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.birth_year}</p>
              </div>
            </div>
            <div className="d-sm-flex flex-sm-row">
              <div className="col-12 col-sm-6 d-flex align-items-center">
                <div className="col result-container">
                  <p >gender:</p>
                </div>
              </div>
              <div className="col-12 col-sm-6 col-md-6 col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                <p >{person.gender}</p>
              </div>
            </div>
          </div>
        ))} 
      </div>
    );
  }
}

export default StarwarsApiResults;