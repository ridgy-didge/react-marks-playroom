import React, { Component } from 'react';
import '../../styles/starwars-app/Starwars.css';

class StarwarsSkinColour extends Component {
  constructor() {
    super();
    this.state = { }
   
  }

  componentDidMount() { }

  render() {
    let { people } = this.props;
    let skinColor = people.slice(0, 1).map( person => (
      person.skin_color
    ))
   
    if (skinColor.length >= 1) {
      let skin = skinColor[0].split(", ");
      console.log(skin);
      console.log(skin[0]);
      console.log(skin[1]);

      var style = {
        backgroundColor: skin[0],
        color: skin[1],
      }
    }
    
   
    
    return (
      <div className="col-12 col-lg-4 justify-content-center skin-results-container">
        <div className="d-flex justify-content-center flex-column">
          <p className="area-title">Skin Colour</p>
          <div className="skin-colour-container" style={style}>
            <p>{skinColor}</p> 
          </div>
        </div>
      </div>
    );
  }}

export default StarwarsSkinColour;