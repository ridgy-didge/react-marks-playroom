import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../styles/wp-products/wp-products.css';


class WPProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productList: [],
    };
  }
  
  componentDidMount() {
    // fetch data and organise state for products

    // const accessKey = "?access_token=t4iea9tUM3QBTQcUGYojRM44";
    const pages = "products?categories=4";   // filter product result to category
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
      .get(
        baseUrl + pages
      )
      .then(res => { // fetch products
        this.setState({ 
          products: res.data
        })
       
        this.state.products.map((product) =>  // loop products to find featured_media id
        
          axios
            .get(
              baseUrl + `media/${product.featured_media}`   // fetch featured_media image data 
            )
            .then(res => { //  add image_url and other properties to product.slug state
              this.setState({ 
                productList: this.state.productList.concat({
                  image_url: res.data.source_url,  // fetch image url from results data
                  title: product.title.rendered,
                  slug: product.slug,
                  excerpt: product.excerpt.rendered,
                  acfDetails: product.acf.product_details,
                  content: product.content.rendered,
                  id: product.id,
                  featured_media: product.featured_media,
                }),  
              })
            })
            .catch(error => console.log(error)),
        )
      })
      .catch(error => console.log(error))
  }


  render() {

    return (
      <div className="App wp-products">
        <div className="App-header">
          <p className="App-description">Wordpress API - custom post type "products" with ACFs</p>
        </div>        
        <div className="container wp-products-container">
            <div className="products">     
              {  this.state.productList.map((product, index) => // loop all products inside ProductList state 
                <div key={product.id + "-" + index} className="products-container">
                  <div className="products-image-container">
                    <div className="products-image-holder">
                      <img src={[product.image_url]} alt={product.title} />
                    </div>
                  </div>
                  <div className="products-title-holder">
                    <h3>{product.title}</h3>     
                  </div>
                  <div className="products-description-holder">
                    <p>{ product.excerpt }</p>
                  </div>
                  <div className="products-link-holder">
                    <Link to={{
                        pathname: `/wp-products/phones/${product.slug}`,
                        state: {
                          productSlug: product.slug,
                          productID: product.id,
                          productTitle: product.title,
                          productMedia: product.featured_media,
                          productImgUrl: product.image_url,
                          productDesc: product.content,
                          acfDetails: product.acfDetails,
                        }
                      }}>
                      {product.title} Info
                    </Link>
                  </div>
                </div>
              )}
            </div>
          
        </div>
      </div>
    )
  }
}

export default WPProducts;
