import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/wp-products/wp-product.css';


// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 

class SingleProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productTitle: this.props.location.state.productTitle,
      productID: this.props.location.state.productID, 
      productSlug: this.props.location.state.productSlug,
      productMedia: this.props.location.state.productMedia,
      productDesc: this.props.location.state.productDesc,
      productAcf: this.props.location.state.acfDetails,
      productColours: [],
      productColour: [],
      productMems: [],
      productImg: this.props.location.state.productImgUrl,
      productPrices: []
    };
  }
  
  render() {
    console.log(this.state)
    return (
      <div className="App wp-product">
        <div className="App-header">
          <p className="App-description">Wordpress API - Product details </p>
        </div>   
        <div className="container product ">
          <div className="product-main">
            <div className="product-header">
              <div className="image-container">
                <img src={this.state.productImg} alt={this.state.productTitle}/>
              </div>
              <div className="product-header-details">
                <div className="product-title-holder">
                  <h2>{this.state.productTitle}</h2>
                </div>
                <div className="product-sku">
                  <p>SKU: {this.state.productID} </p>
                </div>
                <div className="product-description-holder">
                  {this.state.productDesc}
                </div>
              </div>
            </div>
          </div>
            <div className="products-wrapper">
              { this.state.productAcf.map((acfDetails, index) =>
                <div key={index} className="product-container">
                 <svg viewBox="0 0 25 23" className="star rating" data-rating="2">
                    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" fill="rule:nonzero;"/>
                    <text x="7" y="13">Sale</text>
                  </svg>
                  <div className="product-image-container">
                    { acfDetails.product_images.map((images, index) =>
                      <div key={index} className="product-image-holder">
                        <img src={images.image.url} alt={acfDetails.product_title}/>
                       </div>
                    )}
                  </div>
                 
                  <div className="product-details">
                    <div className="product-title-holder">
                      <h2>{this.state.productTitle}</h2>
                    </div>
                    <div className="product-sku">
                      <p>SKU: {this.state.productID}{acfDetails.product_memory[0].memory_options}{acfDetails.product_colours[0].colour} </p>
                    </div>

                    <div className="product-colours-holder option-holder">
                      <div className="title-holder">
                        <p>Colours:</p>
                      </div>
                      <div className="colour-options-holder">
                        { acfDetails.product_colours.map((colours, index) =>
                          <div key={index} className="colour-option">
                            <p>{colours.colour}</p>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="product-memory-holder option-holder">
                      <div className="title-holder">
                        <p>Memory:</p>
                      </div>
                      <div className="memory-options-holder">
                        <div className="memory-option">
                        {acfDetails.product_memory[0].memory_options} GB
                        </div>
                      </div>
                    </div>

                    <h3>Price: £{acfDetails.price}</h3>
                    <div className="product-link-holder">
                      <Link to={{
                          pathname: `/wp-products/phones/${this.state.productSlug}`,
                          state: {
                            //productSlug: product.slug,
                          }
                        }}>
                        Buy {acfDetails.product_title}
                      </Link>
                    </div>
                  </div>
                </div>
              )}
            </div>     
          </div>
        </div>
    );
  }
}

export default SingleProductDetails;

