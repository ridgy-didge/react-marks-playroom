import React, { Component } from 'react';
import axios from 'axios';
import '../styles/all-products-details/all-products-details.css';

import AllColourOptions from './partials/all-colour-options';
import AllMemoryOptions from './partials/all-memory-options';


// import ProductDetails from './product-single-details';

// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 


class AllProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productSlug: []
    };
  }



  componentDidMount() {
    const accessKey = "";
    const pages = "pages/";
    const baseUrl = "https://dev-ridgy-didge.co.uk/wp-rest-api-tests/wp-json/wp/v2/";

    axios
      .get(
        baseUrl + pages + accessKey
      )
      .then(res => {
        this.setState({ 
          products: res.data
        });
      })
      .catch(error => console.log(error));
  }

  render() {
    
    return (
      <div className="all-products-wrapper">
        <div className="App-header">
          <h1 className="App-title">React: Wordpress API, Bootstrap 4 scss</h1>
           <p className="App-description">ACF data included.</p>
        </div>
        <div className="container all-products-container">
          { this.state.products.map((product, index) =>
              product.parent === 75 && // 75 is the product parent page
          <div key={product.parent + "-" + index} className="product">
            <div className="product-header" >
              <div className="product-title-holder">
                <h2>{ product.title.rendered } Options</h2>
              </div>
              <div className="product-description-holder">
                <p>{ product.content.rendered }</p>
              </div>
            </div>
            <div className="products">
              { product.acf.product_details.map((productDetails) =>
              <div key={productDetails.product_title + productDetails.price} className="products-container">
                <div className="products-image-container">
                  <div className="products-image-holder">
                    { productDetails.product_images.map((productImages, index) =>
                      <img key={ productImages.id + index} src={productImages.image.url} alt={product.title.rendered} />
                    )}
                  </div>
                </div>
                <div className="products-details-container">
                  <div className="products-title-holder">
                    <h3>{ product.title.rendered }</h3>
                  </div>
                  <div className="products-options-holder">              
                    <AllMemoryOptions  data={productDetails.product_memory}/>
                    <AllColourOptions  data={productDetails.product_colours}/>
                  </div>
                </div>
                <div className="products-price-holder">
                  <p className="products-price">Price: £{ productDetails.price }</p>
                </div>
              </div>
              )}
            </div>
          </div>
          )}
        </div>
      </div>
    )
  }
}

export default AllProductDetails;
