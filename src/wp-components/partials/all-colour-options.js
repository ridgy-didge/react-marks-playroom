import React, { Component } from 'react';


// import 'bootstrap';    // import all bootstrap js files

// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/tooltip';
// import 'bootstrap/js/dist/util'; 



class ProductColourOptions extends Component {

  render() {
 
    return (
      <div className="product-colours-holder option-holder">
        <div className="title-holder">
          <p>Colour</p>
        </div>
        <div className="colour-options-holder">
          { this.props.data.map((productColours, index) =>
          <div key={index}  className="colour-option">
            {productColours.colour}
          </div>
          )}
         
        </div>
      </div>
    )
  }
}

export default ProductColourOptions;
